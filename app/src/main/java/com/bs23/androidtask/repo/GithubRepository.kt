package com.bs23.androidtask.repo

import android.util.Log
import com.bs23.androidtask.repo.models.GithubApiResponse
import javax.inject.Inject


class GithubRepository @Inject constructor() : BaseRepository(), IGithubRepository {

    override fun getReposByKeyword(
        keyword: String,
        page: Int,
        pageSize: Int,
        dataFetchListener: RepoDataFetchListener<GithubApiResponse>
    ) {
        val param = HashMap<String, String>()
        param["q"] = keyword
        param["page"] = page.toString()
        param["per_page"] = pageSize.toString()
        dataFetchListener.onDataFetching()
        apiHelper.getGithubRepositories(param).subscribe({ result ->
            Log.d("REPO", "getReposByKeyword: " + result.message())
            if (result.code() == 401) {
                dataFetchListener.onFetchError("Unauthorized")
            } else {
                val res = result.body()?.parseResponse(GithubApiResponse::class.java)
                if (res == null) dataFetchListener.onFetchError("NO Data")
                else dataFetchListener.onDataFetched(res)
            }
        }, { err ->
            dataFetchListener.onFetchError(err)
        })
    }
}