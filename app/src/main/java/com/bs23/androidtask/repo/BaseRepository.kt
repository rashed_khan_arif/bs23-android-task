package com.bs23.androidtask.repo

import com.bs23.androidtask.db.AppDBBuilder
import com.bs23.androidtask.network.ApiHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import javax.inject.Inject

open class BaseRepository : IBaseRepository {
    @Inject
    lateinit var apiHelper: ApiHelper

    @Inject
    lateinit var dbBuilder: AppDBBuilder

    @Inject
    lateinit var gson: Gson


    fun <T> ResponseBody.parseResponse(type: Class<T>): T {
        return gson.fromJson(this.string(), type)
    }

}