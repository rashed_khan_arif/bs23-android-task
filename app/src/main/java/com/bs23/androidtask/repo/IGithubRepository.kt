package com.bs23.androidtask.repo

import com.bs23.androidtask.repo.models.GithubApiResponse

interface IGithubRepository {
    fun getReposByKeyword(keyword: String, page: Int, pageSize: Int, dataFetchListener: RepoDataFetchListener<GithubApiResponse>)
}