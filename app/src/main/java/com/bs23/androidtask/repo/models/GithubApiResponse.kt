package com.bs23.androidtask.repo.models

data class GithubApiResponse(
    val total_count: Int = -1,
    val incomplete_status: Boolean = false,
    val items: ArrayList<GitRepoItem> = arrayListOf()
)