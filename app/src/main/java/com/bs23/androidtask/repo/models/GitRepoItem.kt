package com.bs23.androidtask.repo.models

data class GitRepoItem(
    var id: Int = -1,
    val full_name: String = "",
    val url: String = "",
    val topics: ArrayList<String> = arrayListOf(),
    val visibility: Boolean = false,
    val owner: GitRepoOwner? = null

)


data class GitRepoOwner(var id: Int = -1, val avatar_url: String = "")