package com.bs23.androidtask.repo

interface RepoDataFetchListener<T> {
    fun onDataFetching()
    fun onDataFetched(data: T)
    fun onFetchError(error: Any)
}