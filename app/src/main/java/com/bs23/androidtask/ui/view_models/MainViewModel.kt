package com.bs23.androidtask.ui.view_models

import android.util.Log
import androidx.lifecycle.*
import com.bs23.androidtask.repo.IGithubRepository
import com.bs23.androidtask.repo.RepoDataFetchListener
import com.bs23.androidtask.repo.models.GithubApiResponse
import com.bs23.androidtask.util.BaseViewModel
import com.bs23.androidtask.util.LiveDataResult
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val githubRepository: IGithubRepository,
    private val savedState: SavedStateHandle
) : BaseViewModel(), LifecycleEventObserver {

    fun searchGithubRepo(
        lifecycleOwner: LifecycleOwner,
        keyword: String,
        page: Int,
        pageSize: Int
    ): MutableLiveData<LiveDataResult<GithubApiResponse>> {
        val liveData = MutableLiveData<LiveDataResult<GithubApiResponse>>()
        githubRepository.getReposByKeyword(
            keyword,
            page,
            pageSize,
            object : RepoDataFetchListener<GithubApiResponse> {
                override fun onDataFetching() {
                    liveData.postValue(LiveDataResult.loading())
                }

                override fun onDataFetched(data: GithubApiResponse) {
                    liveData.postValue(LiveDataResult.success(data))
                }

                override fun onFetchError(error: Any) {
                    liveData.postValue(LiveDataResult.error(error))
                }
            }

        )
        return liveData;
    }


    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        Log.d("Life Cycle", "Main view lifecycle " + event.name)
        if (event == Lifecycle.Event.ON_RESUME) {
            Log.d("Life Cycle", "Getting repos ")
            searchGithubRepo(source, "Android", 1, 5)
        }
    }

}