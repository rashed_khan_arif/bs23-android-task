package com.bs23.androidtask.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.bs23.androidtask.R
import com.bs23.androidtask.databinding.ActivityMainBinding
import com.bs23.androidtask.ui.adapter.GitrepoAdapter
import com.bs23.androidtask.ui.view_models.MainViewModel
import com.bs23.androidtask.util.LiveDataResult
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private val viewModel: MainViewModel by viewModels()

    lateinit var binding: ActivityMainBinding
    private var PAGE = 1
    private var PER_PAGE = 5
    lateinit var adapter: GitrepoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        lifecycle.addObserver(viewModel)
    }

    override fun viewRelatedTask() {
        viewModel.searchGithubRepo(this, "Android", PAGE, PER_PAGE).observe(this) { result ->
            when (result.status) {
                LiveDataResult.Status.LOADING -> {
                    Log.d("MainActivity", "Loading repos")
                }
                LiveDataResult.Status.SUCCESS -> {
                    Log.d("MainActivity", "Repos found")
                    bindRepo()
                    result.data?.items?.toMutableList()?.let {
                        adapter = GitrepoAdapter(it)
                    }
                }
                LiveDataResult.Status.ERROR -> {
                    Log.d("MainActivity", "Error loading repos")
                }
                else -> {
                    Log.d("MainActivity", "Something went wrong!")
                }
            }
        }
    }

    private fun bindRepo() {
        if (this::adapter.isInitialized) {
            binding.lvRepo.layoutManager = LinearLayoutManager(this)

            binding.lvRepo.adapter = adapter
            binding.lvRepo.itemAnimator = DefaultItemAnimator()
        }
    }

    override fun navigateToHome() {

    }

    override fun onDataLoading(key: String) {

    }

    override fun onDataLoaded(key: String, data: Any) {

    }


}