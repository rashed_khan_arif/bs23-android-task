package com.bs23.androidtask.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.bs23.androidtask.R
import com.bs23.androidtask.core.ConnectionReceiver
import com.bs23.androidtask.network.IDataListener

abstract class BaseActivity : AppCompatActivity(), ConnectionReceiver.ReceiverListener,
    IDataListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ConnectionReceiver.listener = this
        Log.d("Activity:", "OnCreate: Base Activity")
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        viewRelatedTask()
    }


    override fun onNetworkChange(isConnected: Boolean) {
        Log.d("Network:", isConnected.toString())
    }

    abstract fun viewRelatedTask()

    abstract fun navigateToHome()

    override fun onDataLoaded(key: String, data: Any) {}

    override fun onDataLoading(key: String) {}

    override fun onError(key: String, error: Any) {}

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        this.overridePendingTransition(R.anim.activity_in, R.anim.activity_out)
    }

    fun showToast(context: Context, message: String) {
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.custom_toast_layout, null)

        val toastText = view.findViewById<TextView>(R.id.toastText)
        toastText.text = message

        toast.view = view
        toast.show()
    }

    fun hideKeyboard() {
        val inputManager = this.getSystemService(
            Context.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        val focusedView = this.currentFocus

        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(
                focusedView.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }


}