package com.bs23.androidtask.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.bs23.androidtask.db.entities.EntityConstants
import com.bs23.androidtask.db.entities.GithubRepo

@Dao
interface GithubDAO {
    @Query("SELECT * FROM ${EntityConstants.GITHUB_REPO}")
    fun getGithubRepos(): List<GithubRepo>
}