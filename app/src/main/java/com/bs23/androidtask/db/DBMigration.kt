package com.bs23.androidtask.db

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.bs23.androidtask.db.entities.EntityConstants

object DBMigration {
    private val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                "CREATE TABLE `${EntityConstants.GITHUB_REPO}` (`localId` INTEGER NOT NULL DEFAULT 0, " + "PRIMARY KEY(`localId`))"
            )
        }
    }

    fun getMigrations(): Array<Migration> {
        return arrayOf(MIGRATION_1_2)
    }

}