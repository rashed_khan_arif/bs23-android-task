package com.bs23.androidtask.db

import android.content.Context
import androidx.room.Room
import javax.inject.Inject

class AppDBBuilder @Inject constructor(context: Context) {
    private val db =
        Room.databaseBuilder(context, ApplicationDatabase::class.java, "BS_ANDROID")
            .allowMainThreadQueries()
            .addMigrations(
                *DBMigration.getMigrations(),
            ).build()

    fun buildDatabase(): ApplicationDatabase {
        return db
    }
}