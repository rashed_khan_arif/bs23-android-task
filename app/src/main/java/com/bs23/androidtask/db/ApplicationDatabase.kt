package com.bs23.androidtask.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bs23.androidtask.db.dao.GithubDAO
import com.bs23.androidtask.db.entities.GithubRepo

@Database(
    version = 1,
    entities = [GithubRepo::class],
    exportSchema = false
)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun GithubDao(): GithubDAO
}