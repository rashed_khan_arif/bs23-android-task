package com.bs23.androidtask.di

import com.bs23.androidtask.db.entities.GithubRepo
import com.bs23.androidtask.repo.BaseRepository
import com.bs23.androidtask.repo.GithubRepository
import com.bs23.androidtask.repo.IBaseRepository
import com.bs23.androidtask.repo.IGithubRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindBaseRepo(baseRepository: BaseRepository): IBaseRepository

    @Binds
    abstract fun bindGithubRepo(githubRepo: GithubRepository): IGithubRepository

}