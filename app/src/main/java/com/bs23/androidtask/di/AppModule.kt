package com.bs23.androidtask.di

import android.content.Context
import android.util.Log
import androidx.annotation.Keep
import com.bs23.androidtask.BuildConfig
import com.bs23.androidtask.db.AppDBBuilder
import com.bs23.androidtask.manager.PrefManager
import com.bs23.androidtask.network.*
import com.bs23.androidtask.util.GsonDateTimeAdapter
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@Keep
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        retryInterceptor: RetryInterceptor,
        authInterceptor: AuthInterceptor,
        connectivityInterceptor: ConnectivityInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .followRedirects(true)
            .followSslRedirects(true)
            .retryOnConnectionFailure(true)
            .addInterceptor(connectivityInterceptor)
            .addInterceptor(authInterceptor)
            .addInterceptor(retryInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor {
            Log.d("Response->", it)
        }
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    @Singleton
    fun provideAuthInterceptor(@ApplicationContext context: Context) = AuthInterceptor(context)

    @Provides
    @Singleton
    fun provideRetryInterceptor() = RetryInterceptor()

    @Provides
    @Singleton
    fun provideConnectivityInterceptor(@ApplicationContext context: Context) =
        ConnectivityInterceptor(context)

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): IApiService =
        retrofit.create(IApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiService: IApiService): ApiHelper = ApiHelper(apiService)

    @Provides
    @Singleton
    fun provideAppDBBuilder(@ApplicationContext context: Context): AppDBBuilder = AppDBBuilder(context)

    @Provides
    @Singleton
    fun providePrefManager(@ApplicationContext context: Context): PrefManager = PrefManager(context)

    @Provides
    @Singleton
    fun provideGson() = GsonBuilder().apply {
        setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        setDateFormat("yyyy-MM-dd HH:mm:ss")
        registerTypeAdapter(Date::class.java, GsonDateTimeAdapter)
    }.create()
}