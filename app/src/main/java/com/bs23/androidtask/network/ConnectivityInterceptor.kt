package com.bs23.androidtask.network

import android.content.Context
import com.bs23.androidtask.core.Utils
import com.bs23.androidtask.manager.PrefManager
import com.bs23.androidtask.util.NoConnectivityException
import okhttp3.Interceptor
import okhttp3.Response

class ConnectivityInterceptor(var context: Context) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        if (!Utils.isNetworkAvailable(context)) {
            throw NoConnectivityException()
        }
        return chain.proceed(requestBuilder.build())
    }
}