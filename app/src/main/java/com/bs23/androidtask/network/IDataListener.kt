package com.bs23.androidtask.network

interface IDataListener {
    fun onDataLoading(key: String)
    fun onDataLoaded(key: String, data: Any)
    fun onError(key: String, error: Any)
}