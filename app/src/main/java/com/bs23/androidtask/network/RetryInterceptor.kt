package com.bs23.androidtask.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

class RetryInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var response = chain.proceed(chain.request())
        var retryCount = 0;
        while (!response.isSuccessful && retryCount < 4) {
            Log.d("intercept", "Request is not successful - $retryCount")
            retryCount++
            response.close()
            response = chain.proceed(chain.request())
        }
        return response;
    }
}