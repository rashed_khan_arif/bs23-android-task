package com.bs23.androidtask.network

import android.content.Context
import com.bs23.androidtask.manager.PrefManager
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(context: Context) : Interceptor {
    private val prefManager = PrefManager(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        // If token has been saved, add it to the request
        // prefManager.fetchAuthToken()?.let {
//        requestBuilder
//            .addHeader("Content-Type", "application/x-www-form-urlencoded")
//            .addHeader("Authorization", "Bearer ")
        //  }

        return chain.proceed(requestBuilder.build())
    }
}