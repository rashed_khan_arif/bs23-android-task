package com.bs23.androidtask.network

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

class ApiHelper @Inject constructor(private var apiService: IApiService) {

    private fun requestWrapper(request: Maybe<Response<ResponseBody>>) =
        request.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun getGithubRepositories(
        params: Map<String, String>
    ): Maybe<Response<ResponseBody>> {
        return requestWrapper(apiService.getRequest("search/repositories", params))
    }

}