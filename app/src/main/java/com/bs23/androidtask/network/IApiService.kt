package com.bs23.androidtask.network

import io.reactivex.rxjava3.core.Maybe
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface IApiService {

    @GET("{url}")
    fun getRequest(
        @Path(value = "url", encoded = true) path: String,
        @QueryMap hashMap: Map<String, String>
    ): Maybe<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("{url}")
    fun postRequest(
        @Path(value = "url", encoded = true) path: String,
        @FieldMap hashMap: Map<String, String>
    ): Maybe<Response<ResponseBody>>

    @FormUrlEncoded
    @POST("{url}")
    fun postRequestSync(
        @Path(value = "url", encoded = true) path: String,
        @FieldMap hashMap: Map<String, String>
    ): Call<Response<ResponseBody>>

    @Multipart
    @POST("{url}")
    fun sendDocuments(
        @Path(value = "url", encoded = true) path: String,
        @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>
    ): Maybe<Response<ResponseBody>>

    @Multipart
    @POST("{url}")
    fun sendDocuments(
        @Path(value = "url", encoded = true) path: String,
        @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part photo: MultipartBody.Part
    ): Maybe<Response<ResponseBody>>


    @Headers("Content-Type: application/json")
    @POST("{url}")
    fun postRequestForRaw(
        @Path(value = "url", encoded = true) path: String,
        @Body requestBody: RequestBody
    ): Maybe<Response<ResponseBody>>


}