package com.bs23.androidtask.core

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.bs23.androidtask.core.Utils.isNetworkAvailable

class ConnectionReceiver : BroadcastReceiver() {

    companion object {
        var listener: ReceiverListener? = null
    }

    override fun onReceive(context: Context, p1: Intent) {
        if (listener != null) {
            val isConnected = isNetworkAvailable(context)
            listener?.onNetworkChange(isConnected);
        }
    }



    interface ReceiverListener {
        fun onNetworkChange(isConnected: Boolean)
    }
}