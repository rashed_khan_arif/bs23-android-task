package com.bs23.androidtask.util

class LiveDataResult<T>(
    val status: Status,
    val data: T? = null,
    val err: Any? = null,
    val message: String? = null,
    val code: Int? = null
) {
    companion object {
        fun <T> success(data: T?) = LiveDataResult(Status.SUCCESS, data)
        fun <T> loading() = LiveDataResult<T>(Status.LOADING)
        fun <T> error(err: Any?) = LiveDataResult<T>(Status.ERROR, null, err)
        fun <T> error(code: Int?, message: String?, data: T?) =
            LiveDataResult<T>(Status.CODE_ERROR, data, null, message, code)
    }

    enum class Status {
        SUCCESS, ERROR, LOADING, CODE_ERROR
    }

}
