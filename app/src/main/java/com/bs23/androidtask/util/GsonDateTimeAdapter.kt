package com.bs23.androidtask.util

import com.google.gson.*
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object GsonDateTimeAdapter : JsonDeserializer<Date>, JsonSerializer<Date> {
    private val DATE_FORMATS = arrayOf("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd")
    private val dateFormatters: List<SimpleDateFormat> =
        DATE_FORMATS.map { SimpleDateFormat(it, Locale.US) }

    override fun serialize(
        src: Date?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        synchronized(dateFormatters.first()) {
            val dateFormatAsString: String = dateFormatters.first().format(src!!)
            return JsonPrimitive(dateFormatAsString)
        }
    }

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Date {
        for (formatter in dateFormatters) {
            try {
                return formatter.parse(json!!.asString)!!
            } catch (ignore: ParseException) {
                ignore.printStackTrace()
            } catch (ignore: Exception) {
                ignore.printStackTrace()
            }
        }

        throw JsonParseException("DateParseException: " + json.toString())
    }

}