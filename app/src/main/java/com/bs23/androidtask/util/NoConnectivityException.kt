package com.bs23.androidtask.util

import java.io.IOException

class NoConnectivityException() : IOException() {
    override fun getLocalizedMessage(): String {
        return "No internet available"
    }
}